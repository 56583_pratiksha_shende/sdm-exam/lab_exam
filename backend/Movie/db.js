const mysql = require('mysql2')


const pool = mysql.createPool({
  
  host: 'demoDB',

  user: 'root',
  password: 'root',
  database: 'demoDB',
  port: 3306,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
})

module.exports = pool
