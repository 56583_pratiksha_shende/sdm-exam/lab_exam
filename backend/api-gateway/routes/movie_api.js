const express =require('express')
const axios =require('axios').default

const MovieService ='http://localhost:4000'

const router =express.Router()

router.post('/',(request,response)=>{

    axios
    .post(MovieService+'/movie'+request.body)
    .then((axiosResponse)=>{
        response.send(axiosResponse.data)
    })
})

router.get('/',(request,response)=>{

    axios
    .get(MovieService+'/movie')
    .then((axiosResponse)=>{
        response.send(axiosResponse.data)
    })
})

router.put('/:movie_id',(request,response)=>{

    axios
    .post(MovieService+'/movie'+movie_id,request.body)
    .then((axiosResponse)=>{
        response.send(axiosResponse.data)
    })
})

router.delete('/:movie_id',(request,response)=>{

    axios
    .post(MovieService+'/movie'+movie_id,request.body)
    .then((axiosResponse)=>{
        response.send(axiosResponse.data)
    })
})